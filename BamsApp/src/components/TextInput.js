import React, { useState } from 'react';
import { View, Text, TextInput } from 'react-native';

import { height, width } from '../utils/dimension';
import Icon from 'react-native-vector-icons/Entypo';

const TextInputDefault = React.forwardRef((props, ref) => {

	const [isFocused, setFocus] = useState(false);
	const [secureTextEntry, setSecure] = useState(props.secureTextEntry);

	const handleFocus = () => setFocus(true);
	const handleBlur = () => props.value.length === 0 ? setFocus(false) : setFocus(true);

	const labelStyle = {
		top: !isFocused && props.value.length === 0 ? height * 0.04 : height * 0,
		fontSize: !isFocused ? 13.5 : 16,
		color: !isFocused ? 'gray' : 'black'
	};

	const isIconExist = props.nameIcon && props.nameIcon !== '';
	const showIcon = secureTextEntry === false && props.nameIcon === 'eye-with-line' ? 'eye' : props.nameIcon;

	const showText = () => {
		if (secureTextEntry !== undefined) {
			setSecure(!secureTextEntry);
		} else {
			props.onPress();
		}
	};

	return (
		<View style={[{
			alignSelf: 'center'
		}, props.containerStyle]}>
			<Text style={[labelStyle, props.styleText]}>
				{props.label}
			</Text>
			<View>
				<TextInput
					style={[{
						width: width * 0.85,
						height: height * 0.08,
						borderBottomWidth: isFocused ? 2 : 1,
						borderBottomColor: 'purple'
					},
					props.style]}
					onFocus={handleFocus}
					onBlur={handleBlur}
					value={props.value}
					onChangeText={props.onChangeText}
					secureTextEntry={secureTextEntry}
					keyboardType={props.keyboardType}
					onSubmitEditing={props.onSubmitEditing}
					isValid={props.isValid}
					ref={ref}
					editable={props.editable}
				/>
				{isIconExist && (
					<Icon
						name={showIcon}
						size={props.sizeIcon}
						color={'gray'}
						style={[{
							position: 'absolute',
							bottom: 20,
							right: 0
						},
						props.styleTouchable]}
						onPress={showText}
					/>
				)}
			</View>
		</View>
	)
})

export default TextInputDefault;