import React, { useState, useRef } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Button,
  TouchableOpacity,
  Text
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

import { height, width } from '../../utils/dimension';
import TextInput from '../../components/TextInput';
import GradientButton from '../../components/GradientButton';

const SocmedIcon = (props) => {
  return (
    <View style={[styles.socmedIconStyle, props.style]}>
      <Icon name={props.nameIcon} size={30} color={"white"} />
    </View>
  )
};

const LoginScreen = () => {

  const inputEl2 = useRef(null);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <View style={styles.container}>
      <View style={{ width: '100%', height: '30%', backgroundColor: 'blue' }} />
      <View style={{ width: '100%', height: '10%', justifyContent: 'center', alignItems: 'center', borderBottomWidth: 0.5 }}>
        <Text>Nightcore - You Don't Know (Lyrics)</Text>
        <Text>62M views . 3 Years ago</Text>
      </View>
      <View style={{ width: '100%', height: '10%' }}>
        <View style={{ width: 50, height: 50, borderRadius: 25, backgroundColor: 'white' }} />
      </View>
    </View>


    // <View style={styles.container}>

    //   <View style={styles.loginCard}>
    //     <Image
    //       source={require('../../assets/example_logo3.png')}
    //       style={styles.logoImage}
    //     />
    //     <TextInput
    //       value={email}
    //       label={'Email'}
    //       onChangeText={(val) => setEmail(val)}
    //       sizeIcon={20}
    //       nameIcon={'mail'}
    //       onSubmitEditing={() => inputEl2.current.focus()}
    //     />
    //     <TextInput
    //       value={password}
    //       label={'Password'}
    //       onChangeText={(val) => setPassword(val)}
    //       secureTextEntry={true}
    //       sizeIcon={20}
    //       nameIcon={'eye-with-line'}
    //       containerStyle={{ paddingTop: 10 }}
    //       ref={inputEl2}
    //     />
    //   </View>
    //   <View style={styles.diagonal}>

    //   </View>

    //   {/* <GradientButton
    //     text={'Login'}
    //     colors={['gray', 'purple']}
    //   /> */}

    //   <TouchableOpacity style={{ width: '40%', height: '20%', backgroundColor: "blue", alignItems: 'center', justifyContent: 'center' }}>
    //     <Text style={{ textAlign: 'center' }}>INI LOGIN BUTTON</Text>
    //   </TouchableOpacity>

    //   <SocmedIcon
    //     nameIcon={'googleplus'}
    //     style={{
    //       marginTop: height * 0.05,
    //       marginRight: width * 0.6
    //     }}
    //   />

    //   <SocmedIcon
    //     nameIcon={'facebook-square'}
    //     style={{
    //       marginTop: height * 0.02,
    //       marginRight: width * 0.1,
    //       backgroundColor: 'blue'
    //     }}
    //   />

    //   <SocmedIcon
    //     nameIcon={'twitter'}
    //     style={{
    //       marginLeft: width * 0.4,
    //       marginTop: 10,
    //       backgroundColor: 'aqua'
    //     }}
    //   />

    //   <View style={styles.diagonalBottom}>

    //   </View>
    // </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'gray',
    // alignItems: 'center'
  },
  loginCard: {
    backgroundColor: 'white',
    marginTop: height * 0.02,
    height: height * 0.52,
    width: width * 0.95,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  diagonal: {
    width: width * 0.95,
    borderLeftColor: 'transparent',
    borderLeftWidth: width * 0.95,
    borderTopWidth: width * 0.6,
    borderTopColor: 'white',
    marginBottom: height * -0.4
  },
  diagonalBottom: {
    borderRightColor: 'transparent',
    borderRightWidth: width * 0.68,
    borderBottomWidth: width * 0.4,
    borderBottomColor: 'white',
    marginRight: width * 0.28,
    marginTop: height * -0.16
  },
  socmedIconStyle: {
    width: width * 0.125,
    height: width * 0.125,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20
  },
  logoImage: {
    resizeMode: 'contain',
    width: width * 0.3,
    height: height * 0.16,
    alignSelf: "center",
    marginVertical: 10,
    tintColor: 'gray'
  }
});

export default LoginScreen;